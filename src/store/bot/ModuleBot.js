import state from './ModuleStateBot.js'
import getters from './ModuleGettersBot.js'
import mutations from './ModuleMutationsBot.js'
import actions from './ModuleActionsBot.js'

export default {
    state,
    getters,
    mutations,
    actions
}