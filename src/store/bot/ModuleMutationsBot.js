import store from '@/store'
import axios from '@/axios'
export default {
    async StepBotCreate(state,step)
    {
        switch(step)
        {
            case 1:
                state.step = step
                state.chat = 'Chọn dự án?';
                var LIST_DUAN_BOT = []
                await axios.get('/api/du-an?api_token='+axios.defaults.params.api_token).then((response) => {
                    LIST_DUAN_BOT  =  response.data
                    var array_duan =  response.data
                    
                    array_duan.forEach((object) => {
                        setTimeout(() => {
                            state.chat = `${object.id_du_an} : ${object.ten_du_an}`
                        },100)
                    })
                    state.next = 1;
                }).catch((err) => {
                    // console.log('err' , err)
                })
                console.log('test ' , LIST_DUAN_BOT)
                state.LIST_DUAN_BOT = LIST_DUAN_BOT
                state.chat = ''
                break;
        }
    },
    NextStepBotCreate(state,chat)
    {
        switch(state.next)
        {
            case 1:
                console.log('LIST_DUAN_BOT ', state.LIST_DUAN_BOT)
                if(state.LIST_DUAN_BOT.length == 0)
                {
                   state.chat = 'Vui lòng chọn lại'
                }
                else
                {
                    if(Object.entries(state.LIST_DUAN_BOT[chat]).length > 0)
                    {
                        state.cong_viec_bot.id_du_an = state.LIST_DUAN_BOT[chat].id_du_an
                        state.chat = `Bạn đã chọn dự án ${state.LIST_DUAN_BOT[chat].ten_du_an}`
                        setTimeout(() => {
                            state.chat = 'Vui lòng chọn khách hàng?'
                        },100)
                        var LIST_KHACHHANG_BOT = []
                        axios.get('/api/du-an-kh/'+chat+'?show=ID_DU_AN&api_token='+axios.defaults.params.api_token).then((response) => {
                           var array_khachhang = response.data
                           LIST_KHACHHANG_BOT = array_khachhang
                           array_khachhang.forEach((khachang) => {
                               console.log('khachang' , khachang)
                                setTimeout(() => {
                                    state.chat = `${khachang.id_du_an_kh} : ${khachang.ten_kh}`
                                },100)
                           })
                           state.LIST_KHACHHANG_BOT = LIST_KHACHHANG_BOT
                           state.next = 2
                        }).catch(() => {
                            state.chat = 'Vui lòng chọn lại dự án'
                        })
                    }
                    else
                    {
                        state.chat = 'Không tồn tại dụ án này! Vui lòng chọn lại!   '
                    }
                }
                break;
            case 2:
                console.log('TEST ' ,state.LIST_KHACHHANG_BOT)
                if(state.LIST_KHACHHANG_BOT.length == 0)
                {
                    state.chat = 'Vui lòng chọn lại'
                }
                else
                {
                    var khachahng = state.LIST_KHACHHANG_BOT.filter((value,index,array) => {
                        return array[index].id_du_an_kh == chat
                    })[0]
                    if(Object.entries(khachahng).length > 0)
                    {
                        state.chat = 'Bạn vừa chọn khách hàng ' +  khachahng.ten_kh
                        state.cong_viec_bot.id_du_an_kh = khachahng.id_du_an_kh
                        state.next = 3
                    }
                    else    
                    {
                        state.chat = 'Không tồn tại khách hàng này!'
                    }
                }
                break;
            case 3:
                state.chat = 'Chọn loại công việc?'
                var LOAI_CONG_VIEC_BOT = []
                axios.get('/api/loai-cv?api_token='+axios.defaults.params.api_token).then((response) => {
                    LOAI_CONG_VIEC_BOT = response.data
                    LOAI_CONG_VIEC_BOT.forEach((element) => {
                        setTimeout(() => {
                            state.chat = `${element.id_loai_cv} : ${element.ten_loai_cv}`
                        },100)
                    })
                })
                .catch((error) => { 
                    
                 })
          
                break;
        }
    }
}